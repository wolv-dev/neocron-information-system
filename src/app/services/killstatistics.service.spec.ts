import { TestBed } from '@angular/core/testing';

import { KillstatisticsService } from './killstatistics.service';

describe('KillstatisticsService', () => {
  let service: KillstatisticsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KillstatisticsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
