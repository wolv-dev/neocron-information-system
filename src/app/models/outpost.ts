export interface Outpost {
    id?: string;
    coords: string;
    shape: string;
    sector: string;
    title: string;
    owner?: string;
    faction?: string;
    conquertime?: Date;
    enemyid?: string;
    securitysetting?: string;
    formerowner?: string;
}
