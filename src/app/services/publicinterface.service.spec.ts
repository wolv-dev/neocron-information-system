import { TestBed } from '@angular/core/testing';

import { PublicinterfaceService } from './publicinterface.service';

describe('PublicinterfaceService', () => {
  let service: PublicinterfaceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PublicinterfaceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
